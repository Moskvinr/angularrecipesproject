﻿using RazorEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RazorEngine.Templating;

namespace Sprashivai.ru
{
    public class BridgePattern
    {
        public DataBase db { get; set; }
        public Sessions sessions { get; set; }
        public string Login { get; set; }
        public string GuestLogin { get; set; }

        public BridgePattern()
        {
            db = new DataBase();
            sessions = new Sessions();
        }

        public string Reader(string pageName)
        {
            using (var sr = new StreamReader(pageName))
                return sr.ReadToEnd();
        }

        public string ReturnMainPage()
        {
            var result = Reader("Pages/Page.cshtml");
            var questions = db.GetAllQuestions(Login);
            var status = db.GetStatus(Login);
            var imagepath = db.GetImagePath(Login);
            var userinfo = db.GetUserInfo(Login);
            return Engine.Razor.RunCompile(result, "result", null, new
            {
                Login = Login,
                Count = questions.Count,
                Questions = questions,
                Status = status,
                ImagePath = imagepath,
                UserInfo = userinfo
            });
        }

        public string ReturnMainPageForGuests()
        {
            var result = Reader("Pages/GuestPage.cshtml");
            var questions = db.GetAllQuestionsWithAnswears(GuestLogin);
            var status = db.GetStatus(GuestLogin);
            var imagepath = db.GetImagePath(GuestLogin);
            var userinfo = db.GetUserInfo(GuestLogin);
            return Engine.Razor.RunCompile(result, "resultGuest", null, new
            {
                Login = GuestLogin,
                Count = questions.Count,
                Questions = questions,
                Status = status,
                ImagePath = imagepath,
                UserInfo = userinfo
            });
        }


    }
}
