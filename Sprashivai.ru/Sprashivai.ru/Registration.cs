﻿using HttpMultipartParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Sprashivai.ru
{
    public class RegistrationTreat
    {
        public BridgePattern bridge;
        public RegistrationTreat(BridgePattern bridge)
        {
            this.bridge = bridge;
        }

        public async Task Registration()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/FullRegistration/");
            listener.Start();

            while (true)
            {
                var context = await listener.GetContextAsync();
                var request = context.Request;
                var response = context.Response;
                if (request.RawUrl.StartsWith("/static"))
                {
                    CheckForStatic(request, response);
                    continue;
                }

                string responseStr = RegistrationProccess(context);

                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseStr);
                response.ContentLength64 = buffer.Length;

                var os = response.OutputStream;
                os.Write(buffer, 0, buffer.Length);
                os.Close();
            }
            listener.Stop();
        }

        private void CheckForStatic(HttpListenerRequest request, HttpListenerResponse response)
        {
            var file = File.ReadAllBytes(Directory.GetCurrentDirectory() + request.RawUrl);
            response.ContentLength64 = file.Length;
            response.OutputStream.Write(file, 0, file.Length);
            response.OutputStream.Close();
        }

        private string RegistrationProccess(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            if (context.Request.HttpMethod == "GET")
                return bridge.Reader("Pages/RegistrationsPage.cshtml");

            if (context.Request.HttpMethod == "POST")
            {
                var parser = new MultipartFormDataParser(request.InputStream, request.ContentEncoding);
                var firstName = parser.GetParameterValue("firstName");
                var secondName = parser.GetParameterValue("secondName");
                var sex = parser.GetParameterValue("sex");
                Console.WriteLine(WebUtility.UrlDecode(firstName));
                Console.WriteLine(WebUtility.UrlDecode(secondName));
                Console.WriteLine(WebUtility.UrlDecode(sex));
                var date = DateTime.Parse(parser.GetParameterValue("name"));
                bridge.db.AddUserInfo(bridge.Login, firstName, secondName, date);
                response.Redirect("http://localhost:8080/");
                return bridge.Reader("Pages/FirstPage.cshtml");
            }
            return null;
        }
    }
}
