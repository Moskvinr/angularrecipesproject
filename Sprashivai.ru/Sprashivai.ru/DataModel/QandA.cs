﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Sprashivai.ru.DataModel
{
    public class QandA
    {
        [Key]
        public int IdQA { get; set; }
        public int UserId { get; set; }
        public string ToLogin { get; set; }
        public string FromLogin { get; set; }
        public string Question { get; set; }
        public string Answear { get; set; }
        public User User { get; set; }
        
    }
}
