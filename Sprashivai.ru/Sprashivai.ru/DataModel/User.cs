﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Sprashivai.ru.DataModel
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Login { get; set; }
        public int Password { get; set; }
        public string Email { get; set; }

        public virtual UserInfo UserInfo { get; set; }

        public virtual List<QandA> Questions { get; set; }
        //public virtual QandA Questions { get; set; }
    }
}
