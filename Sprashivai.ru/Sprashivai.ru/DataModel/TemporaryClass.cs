﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprashivai.ru.DataModel
{

    public class TemporaryClass
    {
        public int IdQA { get; set; }
        public int UserId { get; set; }
        public string ToLogin { get; set; }
        public string FromLogin { get; set; }
        public string Question { get; set; }
        public string Answear { get; set; }
        public string ImagePath { get; set; }
    }

}
