﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Sprashivai.ru.DataModel
{
    public class UserInfo
    {
        [Key]
        public int InfoId { get; set; }

        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte Age { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        public string PicturePath { get; set; }
        
    }
}
