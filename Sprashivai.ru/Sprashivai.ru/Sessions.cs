﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sprashivai.ru.DataModel;
using System.Net;
using System.Timers;
using System.Security.Cryptography;

namespace Sprashivai.ru
{
    class Session
    {
        public string Id { get; set; }
        public User User { get; set; }
        public DateTime Expires { get; set; }
    }

    public class Sessions
    {
        Dictionary<string, Session> CoockiesAndSessions;

        public Sessions()
        {
            CoockiesAndSessions = new Dictionary<string, Session>();
        }

        public void AddCookie(HttpListenerResponse response, string Login)
        {
            using (var context = new UserContext())
            {
                Session session = new Session
                {
                    User = context.Users
                    .Where(x => x.Login == Login)
                    .Select(x => x)
                    .FirstOrDefault(),
                    Expires = DateTime.UtcNow.AddMinutes(30)
                };
                var hasher = SHA1.Create();
                hasher.ComputeHash(Encoding.UTF8.GetBytes(session.GetHashCode().ToString()));
                string id = Convert.ToBase64String(hasher.Hash).Trim(new char[] { ',', ' ', '=', ';', '\n', '\t', '\r' });
                session.Id = id;
                CoockiesAndSessions.Add(id, session);
                response.SetCookie(new Cookie
                {
                    Name = id,
                    Expires = Convert.ToDateTime((DateTime.UtcNow.AddMinutes(30).ToString("ddd,dd-MMM,yyy H:mm:ss") + "GMT"))
                });

            }
        }

        public bool CheckCookie(HttpListenerRequest request)
        {
            foreach (Cookie coock in request.Cookies)
                foreach (var dict in CoockiesAndSessions)
                    if (dict.Key == coock.Name)
                        return true;
            return false;
        }

        public void ShowAllCookies(HttpListenerRequest request)
        {
            foreach (Cookie coock in request.Cookies)
                Console.WriteLine($"{coock.Comment} {coock.Name}");
        }

        public void DeleteCookie(HttpListenerContext context,string login)
        {
            //string login = GetLogin(context.Response);
            var id = CoockiesAndSessions
                .Where(x => x.Value.User.Login == login)
                .Select(x => x.Key)
                .FirstOrDefault();
            foreach (Cookie cookie in context.Request.Cookies)
                if (cookie.Name == id)
                    cookie.Expires = Convert.ToDateTime((DateTime.UtcNow.AddDays(-1).ToString("ddd,dd-MMM,yyy H:mm:ss") + "GMT"));
            try
            {
                CoockiesAndSessions.Remove(id);
            }
            catch
            {

            }
        }

        public string GetLogin(HttpListenerContext context)
        {
            return CoockiesAndSessions
                .Where(x => x.Key == context.Request.Cookies[context.Request.Cookies.Count - 1].Name)
                .Select(x => x.Value)
                .FirstOrDefault()
                .User
                .Login;
        }


        //https://gist.github.com/bgrins/1789787
        //https://github.com/Vodurden/Http-Multipart-Data-Parser/blob/master/HttpMultipartParser/MultipartFormDataParser.cs

    }
}
