﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using Sprashivai.ru.DataModel;
using System.Net;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;

namespace Sprashivai.ru
{
    public class DataBase
    {
        public UserContext context { get; private set; }
        public DataBase()
        {
            this.context = new UserContext();
        }

        public string GetImagePath(string Login)
        {
            return context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault()
                .PicturePath;
        }

        public void AddImagePath(string Login, string path)
        {
            context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault()
                .PicturePath = path;
            context.SaveChanges();
        }

        public string GetStatus(string login)
        {
            using (var specialContext = new UserContext())
            {
                return specialContext.Users
                .Where(x => x.Login == login)
                .Select(x => x.UserInfo)
                .FirstOrDefault()
                .Status;
            }
        }

        public bool CheckPass(string Login, int Password)
        {
            if (!Search(Login))
                return false;
            if (context.Users.Where(x => x.Login == Login).FirstOrDefault().Password == Password)
                return true;
            return false;
        }

        public void AddNewUser(string Login, int Password, string Email)
        {
            var users = context.Users;
            if (users.Where(x => x.Login == Login).Count() == 0)
            {
                context.Users.Add(new User
                {
                    Login = Login,
                    Password = Password,
                    Email = Email,
                    UserInfo = new UserInfo
                    {
                        Status = "default"
                    }
                });
                context.SaveChanges();
            }
            else
            {
                throw new Exception("qwe");
            }
        }

        public void AddPrivateQuestion(string LoginTo, string Question)
        {
            var userId = context.Users
                .Where(x => x.Login == LoginTo)
                .Select(x => x.UserId).FirstOrDefault();
            context.QuestionsAnswears
                .Add(new QandA
                {
                    UserId = userId,
                    Question = Question,
                    ToLogin = LoginTo,
                    FromLogin = "Anonymous"
                });

            context.SaveChanges();
        }

        public void AddPublicQuestion(string LoginTo, string LoginFrom, string Question)
        {
            var idqa = context.Users
                .Where(x => x.Login == LoginTo)
                .Select(x => x.UserId)
                .FirstOrDefault();
            var id = context.QuestionsAnswears
                .Where(x => x.UserId == idqa)
                .Count() + 1;

            var userId = context.Users
                .Where(x => x.Login == LoginTo)
                .Select(x => x.UserId).FirstOrDefault();
            context.QuestionsAnswears
                .Add(new QandA
                {
                    UserId = idqa,
                    Question = Question,
                    ToLogin = LoginTo,
                    FromLogin = LoginFrom
                });
            context.SaveChanges();
        }

        public void AddAnswear(int Id, string LoginTo, string Answear)
        {
            //using (var context = new UserContext())
            //{
            context.Users
                .Join(context.QuestionsAnswears, x => x.UserId, y => y.UserId, (x, y) => new { x, y })
                .Where(x => x.x.Login == LoginTo && x.y.IdQA == Id)
                .FirstOrDefault()
                .y.Answear = Answear;
            context.SaveChanges();
            //}
        }

        public void AddUserInfo(string Login, string firstname, string secondname, DateTime dateTime)
        {
            //using (var context = new UserContext())
            //{
            var info = context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault();
            if (firstname != null)
                info.FirstName = firstname;
            if (secondname != null)
                info.SecondName = secondname;
            if (dateTime != null)
            {
                TimeSpan span = DateTime.Now - dateTime;
                info.Age = (byte)((new DateTime(1, 1, 1) + span).Year - 1);
            }
            context.SaveChanges();
            //}
        }

        public void AddStatus(string login, string status)
        {
            using (var context = new UserContext())
            {
                context.Users
                    .Where(x => x.Login == login)
                    .Select(x => x.UserInfo)
                    .FirstOrDefault()
                    .Status = status;
                context.SaveChanges();
            }
        }

        public bool Search(string login)
        {
            return context.Users.Any(x => x.Login == login);
        }

        public List<UserInfo> GetUserInfo(string Login)
        {
            return context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .ToList();
        }

        public List<User> GetAllUsers()
        {
            return context.Users.ToList();
        }

        public List<TemporaryClass> GetAllQuestions(string Login)
        {

            return context.Users
                    .Where(x => x.Login == Login)
                    .Join(context.QuestionsAnswears, x => x.UserId, y => y.UserId, (x, y) => y)
                    .OrderByDescending(x => x.IdQA)
                    .Select(x => new TemporaryClass
                    {
                        ImagePath =
                            context.Users.Where(y => y.Login == x.FromLogin)
                            .Select(z => z.UserInfo)
                            .FirstOrDefault()
                            .PicturePath,
                        Answear = x.Answear,
                        FromLogin = x.FromLogin,
                        IdQA = x.IdQA,
                        Question = x.Question,
                        ToLogin = x.ToLogin,
                        UserId = x.UserId
                    })
                    .ToList();
        }

        public List<TemporaryClass> GetAllQuestionsWithAnswears(string Login)
        {

            return context.Users
                    .Where(x => x.Login == Login)
                    .Join(context.QuestionsAnswears, x => x.UserId, y => y.UserId, (x, y) => y)
                    .Where(x => x.Answear != null)
                    .OrderByDescending(x => x.IdQA)
                    .Select(x => new TemporaryClass
                    {
                        ImagePath =
                            context.Users.Where(y => y.Login == x.FromLogin)
                            .Select(z => z.UserInfo)
                            .FirstOrDefault()
                            .PicturePath,
                        Answear = x.Answear,
                        FromLogin = x.FromLogin,
                        IdQA = x.IdQA,
                        Question = x.Question,
                        ToLogin = x.ToLogin,
                        UserId = x.UserId
                    })
                    .ToList();
        }

        public string GetJsonUsers(int id = 0, string written = "")
        {
            var context = new UserContext();
            var res = GetUsersInJSON();
            if (id == 1)
                res = GetUsersInJSON(GetOrderedByAnswearsCount());
            if (id == 2)
                res = res.OrderBy(x => x.Login).ToList();
            if (id == 3)
                res = res.Where(x => x.Login.Contains(written)).Select(x => x).ToList();
            if (id == 4)
                res = GetUsersInJSON(context
                    .QuestionsAnswears
                    .Where(x => x.Question.Contains(written))
                    .Select(x => x.User)
                    .ToList());
            if (id == 5)
                res = GetUsersInJSON(context
                    .QuestionsAnswears
                    .Where(x => x.Answear.Contains(written))
                    .Select(x => x.User)
                    .ToList());
            return JsonConvert.SerializeObject(res, Formatting.Indented);

        }

        public List<User> GetOrderedByAnswearsCount()
        {
            return GetAllUsers()
                    .OrderByDescending(x => x.Questions.Select(y => y.Answear).Count())
                    .ThenBy(x => x.Login)
                    .ToList();
        }

        private List<Jsons> GetUsersInJSON(List<User> users = null)
        {
            List<User> user = GetAllUsers();
            if (users != null)
                user = users;
            return user.Select(x => new Jsons
            {
                Age = x.UserInfo.Age,
                FirstName = x.UserInfo.FirstName,
                Login = x.Login,
                PicturePath = x.UserInfo.PicturePath,
                SecondName = x.UserInfo.SecondName,
                Sex = x.UserInfo.Sex
            })
            .ToList();
        }
    }
}
