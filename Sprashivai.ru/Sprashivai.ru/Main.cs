﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Sprashivai.ru
{
    public class MainTreatment
    {

        private HttpListenerContext listenerContext;
        private string input;
        public BridgePattern bridge;
        private Dictionary<string, Func<string,string>> InputAction;

        public MainTreatment(BridgePattern bridge)
        {
            this.bridge = bridge;
            InputAction = new Dictionary<string, Func<string,string>>()
            {
                { "operation=enter",arg=> Enter() },
                { "Exit=&operation=topButtons", arg=> Exit()},
                { "operation=ask", arg=> Ask() },
                { "addAnswear", arg=> AddAnswear() },
                { "status=change", arg=>ChangeStatus() },
                { "Main=&operation=topButtons", arg=>ReturnToMain() }
            };
        }

        private string ChangeStatus()
        {
            var status = WebUtility.UrlDecode(input).Split('&')[0].Split('=')[1];
            bridge.db.AddStatus(bridge.Login, status);
            return bridge.ReturnMainPage();
        }

        private string AddAnswear()
        {
            var inp = WebUtility.UrlDecode(input).Split('&');
            var qId = Convert.ToInt32(inp[0].Split('=')[1]);
            var answear = inp[1].Split('=')[1];
            bridge.db.AddAnswear(qId, bridge.Login, answear);
            return bridge.ReturnMainPage();
        }

        private string Ask()
        {
            var question = WebUtility.UrlDecode(input).Split('&')[1].Split('=')[1];
            if (input.Contains("checkbox"))
                bridge.db.AddPrivateQuestion(bridge.GuestLogin, question);
            else
                bridge.db.AddPublicQuestion(bridge.GuestLogin, bridge.Login, question);
            return bridge.ReturnMainPageForGuests();
        }
        
        private string Exit()
        {
            bridge.sessions.DeleteCookie(listenerContext, bridge.Login);
            return bridge.Reader("Pages/FirstPage.cshtml");
        }

        private string ReturnToMain()
        {
            return bridge.ReturnMainPage();
        }

        private string Enter()
        {
            var logpass = input.Split('&');
            string Login = logpass[0].Split('=')[1];
            var Password = logpass[1].Split('=')[1].GetHashCode();
            if (bridge.db.CheckPass(Login, Password))
            {
                bridge.sessions.AddCookie(listenerContext.Response, Login);
                bridge.Login = Login;
                return bridge.ReturnMainPage();
            }
            else
                return bridge.Reader("Pages/FirstPage.cshtml");
        }

        public async Task Main()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Start();

            while (true)
            {
                var context = await listener.GetContextAsync();
                var request = context.Request;
                var response = context.Response;



                string responseStr;
                var userlogin = request.RawUrl.Split('/').Last();
                if (userlogin != null && bridge.db.Search(userlogin) && bridge.Login != userlogin)
                {
                    bridge.GuestLogin = userlogin;
                    responseStr = bridge.ReturnMainPageForGuests();
                }
                else
                    responseStr = Treat(context);
                if (request.RawUrl.StartsWith("/static"))
                {
                    CheckForStatic(request, response);
                    continue;
                }


                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseStr);
                response.ContentLength64 = buffer.Length;

                var os = response.OutputStream;
                os.Write(buffer, 0, buffer.Length);
                os.Close();
            }
            listener.Stop();
        }

        private void CheckForStatic(HttpListenerRequest request, HttpListenerResponse response)
        {
            var file = File.ReadAllBytes(Directory.GetCurrentDirectory() + request.RawUrl);
            response.ContentLength64 = file.Length;
            response.OutputStream.Write(file, 0, file.Length);
            response.OutputStream.Close();
        }

        private string Treat(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;

            listenerContext = context;

            if (request.HttpMethod == "GET")
                return bridge.sessions.CheckCookie(request) ? bridge.ReturnMainPage() : bridge.Reader("Pages/FirstPage.cshtml");

            if (request.HttpMethod == "POST")
            {
                using (var sr = new StreamReader(request.InputStream, request.ContentEncoding))
                {
                    input = sr.ReadToEnd();
                    
                    if (input.Contains("operation=settings"))
                        response.Redirect("http://localhost:8080/Settings/");

                    if (input.Contains("search"))
                        response.Redirect("http://localhost:8080/AllUsers/");

                    if (input.Contains("operation=registration"))
                    {
                        var reg = input.Split('&');
                        var login = reg[0].Split('=')[1];
                        var password = reg[2].Split('=')[1];
                        var email = reg[1].Split('=')[1];
                        bridge.Login = login;
                        bridge.db.AddNewUser(login, password.GetHashCode(), email);
                        response.Redirect("http://localhost:8080/FullRegistration/");
                    }

                    foreach(var x in InputAction)
                        if (input.Contains(x.Key))
                            return x.Value("");
                }
            }
            return bridge.ReturnMainPage();
        }

    }
}
