namespace Sprashivai.ru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.QandAs",
                c => new
                    {
                        IdQA = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ToLogin = c.String(),
                        FromLogin = c.String(),
                        Question = c.String(),
                        Answear = c.String(),
                    })
                .PrimaryKey(t => t.IdQA)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Login = c.String(),
                        Password = c.Int(nullable: false),
                        Email = c.String(),
                        UserInfo_InfoId = c.Int(),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.UserInfoes", t => t.UserInfo_InfoId)
                .Index(t => t.UserInfo_InfoId);
            
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        InfoId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        Age = c.Byte(nullable: false),
                        Sex = c.String(),
                        Status = c.String(),
                        PicturePath = c.String(),
                    })
                .PrimaryKey(t => t.InfoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UserInfo_InfoId", "dbo.UserInfoes");
            DropForeignKey("dbo.QandAs", "UserId", "dbo.Users");
            DropIndex("dbo.Users", new[] { "UserInfo_InfoId" });
            DropIndex("dbo.QandAs", new[] { "UserId" });
            DropTable("dbo.UserInfoes");
            DropTable("dbo.Users");
            DropTable("dbo.QandAs");
        }
    }
}
