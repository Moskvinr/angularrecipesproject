﻿using HttpMultipartParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Sprashivai.ru
{
    public class SettingsTreatment
    {
        public BridgePattern bridge;
        public SettingsTreatment(BridgePattern bridge)
        {
            this.bridge = bridge;
        }

        public async Task Settings()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/Settings/");
            listener.Start();

            while (true)
            {
                var context = await listener.GetContextAsync();
                var request = context.Request;
                var response = context.Response;

                if (request.RawUrl.StartsWith("/static"))
                {
                    CheckForStatic(request, response);
                    continue;
                }

                string responseStr = SettingsTreat(context);

                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseStr);
                response.ContentLength64 = buffer.Length;

                var os = response.OutputStream;
                os.Write(buffer, 0, buffer.Length);
                os.Close();
            }
            listener.Stop();
        }

        private void CheckForStatic(HttpListenerRequest request, HttpListenerResponse response)
        {
            var file = File.ReadAllBytes(Directory.GetCurrentDirectory() + request.RawUrl);
            response.ContentLength64 = file.Length;
            response.OutputStream.Write(file, 0, file.Length);
            response.OutputStream.Close();
        }

        private string SettingsTreat(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;
            if (request.HttpMethod == "GET")
                return bridge.sessions.CheckCookie(request) ? bridge.Reader("Pages/Settings.cshtml") : bridge.Reader("Pages/FirstPage.cshtml");
            if (request.HttpMethod == "POST")
            {

                var parser = new MultipartFormDataParser(request.InputStream, request.ContentEncoding);
                AddFile(parser, context);
                var firstName = parser.GetParameterValue("firstName");
                var secondName = parser.GetParameterValue("secondName");
                var date = DateTime.Parse(parser.GetParameterValue("name"));

                bridge.db.AddUserInfo(bridge.sessions.GetLogin(context), firstName, secondName, date);
                response.Redirect("http://localhost:8080/");
                return bridge.ReturnMainPage();
            }
            return null;
        }

        private void AddFile(MultipartFormDataParser parser, HttpListenerContext context)
        {
            var file = parser.Files.FirstOrDefault();
            string filename = file.FileName;
            var data = file.Data;
            var directory = "\\static\\images\\" + filename;

            bridge.db.AddImagePath(bridge.Login, directory);

            using (var br = new BinaryReader(data))
                File.WriteAllBytes(Directory.GetCurrentDirectory() + directory, br.ReadBytes((int)data.Length));
        }

    }
}
