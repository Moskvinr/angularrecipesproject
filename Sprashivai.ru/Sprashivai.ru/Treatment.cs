﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Compilation;
using RazorEngine.Templating;
using System.Web;
using System.Threading.Tasks;
using HttpMultipartParser;


namespace Sprashivai.ru
{
    class Treatment
    {
        public Treatment()
        {
            BridgePattern bridge = new BridgePattern();

            SettingsTreatment settingsTreatment = new SettingsTreatment(bridge);
            MainTreatment mainTreatment = new MainTreatment(bridge);
            RegistrationTreat registrationTreat = new RegistrationTreat(bridge);
            CleverSearch cleverSearch = new CleverSearch(bridge);

            List<Task> actions = new List<Task>
            {
                settingsTreatment.Settings(),
                mainTreatment.Main(),
                registrationTreat.Registration(),
                cleverSearch.cleverSearch()
            };

            foreach (var x in actions)
                x.Wait();
        }
    }
}
