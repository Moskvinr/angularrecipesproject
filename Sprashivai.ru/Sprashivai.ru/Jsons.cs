﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Sprashivai.ru.DataModel;

namespace Sprashivai.ru
{
    class Jsons
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte Age { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        public string PicturePath { get; set; }
    }
}
