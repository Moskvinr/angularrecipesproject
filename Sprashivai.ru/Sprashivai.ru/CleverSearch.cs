﻿using RazorEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using RazorEngine.Templating;

namespace Sprashivai.ru
{
    class CleverSearch
    {
        public BridgePattern bridge;
        public CleverSearch(BridgePattern bridge)
        {
            this.bridge = bridge;
        }

        public async Task cleverSearch()
        {
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://localhost:8080/AllUsers/");
            listener.Start();

            while (true)
            {
                var context = await listener.GetContextAsync();
                var request = context.Request;
                var response = context.Response;
                if (request.RawUrl.StartsWith("/static"))
                {
                    CheckForStatic(request, response);
                    continue;
                }
                string responseStr = UsersPageTreat(context);


                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseStr);
                response.ContentLength64 = buffer.Length;

                var os = response.OutputStream;
                os.Write(buffer, 0, buffer.Length);
                os.Close();
            }
            listener.Stop();
        }
        
        private void CheckForStatic(HttpListenerRequest request, HttpListenerResponse response)
        {
            var file = File.ReadAllBytes(Directory.GetCurrentDirectory() + request.RawUrl);
            response.ContentLength64 = file.Length;
            response.OutputStream.Write(file, 0, file.Length);
            response.OutputStream.Close();
        }

        private string UsersPageTreat(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;

            if (request.HttpMethod == "GET")
            {
                if (bridge.sessions.CheckCookie(request))
                {
                    var users = bridge.db.GetOrderedByAnswearsCount();
                    string res = bridge.Reader("Pages/AllUsers.cshtml");
                    return Engine.Razor.RunCompile(res, "allusers", null, new
                    {
                        Users = users
                    });
                }
                return bridge.Reader("Pages/FirstPage.cshtml");
            }

            if (request.HttpMethod == "POST")
            {
                using (var sr = new StreamReader(request.InputStream, request.ContentEncoding))
                {
                    var read = sr.ReadToEnd();
                    var input = read.Split('&');
                    string written;
                    if (input[0].Contains("written"))
                    {
                        written = input[0].Split('=')[1];
                        Console.WriteLine(input[0]);
                        Console.WriteLine(input[1]);
                        return bridge.db.GetJsonUsers(Convert.ToInt32(input[1].Split('=').Last()), written);
                    }
                    return bridge.db.GetJsonUsers(Convert.ToInt32(input[1].Split('=').Last()));
                }
            }
            return null;
        }
    }
}
