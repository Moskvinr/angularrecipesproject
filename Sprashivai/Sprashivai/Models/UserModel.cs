﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai.Models
{
    public class UserModel
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte Age { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        public string PicturePath { get; set; }
    }
}
