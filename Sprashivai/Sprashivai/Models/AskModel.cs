﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai.Models
{
    public class AskModel
    {
        public string LoginTo { get; set; }
        [Required]
        public string Question { get; set; }
        public bool IsAnonymous { get; set; }
    }
}
