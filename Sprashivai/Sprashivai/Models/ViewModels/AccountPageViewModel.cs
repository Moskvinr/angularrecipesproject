﻿using Sprashivai.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai.Models.ViewModels
{
    public class AccountPageViewModel
    {
        public string Login { get; set; }
        public int Count { get; set; }
        public List<QuestionsModel> Questions { get; set; }
        public string Status { get; set; }
        public string ImagePath { get; set; }
        public UserInfo UserInfo { get; set; }
    }
}
