﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai.Models.ViewModels
{
    public class UserInfoViewModel
    {
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        public IFormFile PicturePath { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
