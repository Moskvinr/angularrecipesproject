﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai
{
    public class FilesHandler
    {
        public string CreateFile(IFormFile file)
        {
            var currDir = Directory.GetCurrentDirectory();
            var imagesDir = Path.Combine(currDir, "Images");
            Directory.CreateDirectory(imagesDir);
            var newName = Guid.NewGuid() + file.Name.Split('.').Last();
            using (var stream = new FileStream(Path.Combine(imagesDir, newName), FileMode.Create))
            {
                file.CopyTo(stream);
            }
            return newName;
        }
    }
}
