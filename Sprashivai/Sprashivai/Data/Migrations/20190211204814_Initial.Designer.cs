﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Sprashivai.Data;

namespace Sprashivai.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20190211204814_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.1-rtm-30846")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Sprashivai.Data.Entities.QandA", b =>
                {
                    b.Property<int>("IdQA")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Answear");

                    b.Property<string>("FromLogin");

                    b.Property<string>("Question");

                    b.Property<string>("ToLogin");

                    b.Property<int>("UserId");

                    b.HasKey("IdQA");

                    b.HasIndex("UserId");

                    b.ToTable("QuestionsAnswears");
                });

            modelBuilder.Entity("Sprashivai.Data.Entities.User", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email");

                    b.Property<string>("Login");

                    b.Property<int>("Password");

                    b.Property<int?>("UserInfoInfoId");

                    b.HasKey("UserId");

                    b.HasIndex("UserInfoInfoId");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Sprashivai.Data.Entities.UserInfo", b =>
                {
                    b.Property<int>("InfoId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<byte>("Age");

                    b.Property<string>("FirstName");

                    b.Property<string>("PicturePath");

                    b.Property<string>("SecondName");

                    b.Property<string>("Sex");

                    b.Property<string>("Status");

                    b.HasKey("InfoId");

                    b.ToTable("UsersInfo");
                });

            modelBuilder.Entity("Sprashivai.Data.Entities.QandA", b =>
                {
                    b.HasOne("Sprashivai.Data.Entities.User", "User")
                        .WithMany("Questions")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Sprashivai.Data.Entities.User", b =>
                {
                    b.HasOne("Sprashivai.Data.Entities.UserInfo", "UserInfo")
                        .WithMany()
                        .HasForeignKey("UserInfoInfoId");
                });
#pragma warning restore 612, 618
        }
    }
}
