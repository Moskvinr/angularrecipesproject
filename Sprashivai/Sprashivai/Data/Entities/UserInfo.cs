﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai.Data.Entities
{
    public class UserInfo
    {
        [Key]
        public int InfoId { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public byte Age { get; set; }
        public string Sex { get; set; }
        public string Status { get; set; }
        public string PicturePath { get; set; }
    }
}
