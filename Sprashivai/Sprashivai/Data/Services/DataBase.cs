﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Sprashivai.Data.Entities;
using Sprashivai.Models;
using Sprashivai.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sprashivai.Data.Services
{
    public class DataBase
    {
        public ApplicationDbContext context { get; private set; }
        public DataBase(ApplicationDbContext applicationDbContext)
        {
            this.context = applicationDbContext;
        }

        public string GetImagePath(string Login)
        {
            return context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault()
                .PicturePath;
        }

        internal User GetUser(string login)
        {
            return context
                .Users
                .Include(x => x.UserInfo)
                .FirstOrDefault(x => x.Login == login);
        }

        public void AddImagePath(string Login, string path)
        {
            context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault()
                .PicturePath = path;
            context.SaveChanges();
        }

        public string GetStatus(string login)
        {
            return context.Users
            .Where(x => x.Login == login)
            .Select(x => x.UserInfo)
            .FirstOrDefault()
            .Status;

        }

        public bool CheckIfUserExists(string login)
        {
            return context
                .Users
                .Any(x => x.Login == login);
        }

        public bool CheckPass(string Login, string Password)
        {
            if (!Search(Login))
                return false;
            if (context.Users.Where(x => x.Login == Login).FirstOrDefault().Password == Password)
                return true;
            return false;
        }

        public void AddNewUser(RegistrationViewModel registerModel)
        {
            var users = context.Users;
            if (users.Where(x => x.Login == registerModel.Login).Count() == 0)
            {
                context.Users.Add(new User
                {
                    Login = registerModel.Login,
                    Password = registerModel.Password,
                    Email = registerModel.Email,
                    UserInfo = new UserInfo
                    {
                        Status = "default"
                    }
                });
                context.SaveChanges();
            }
            else
            {
                throw new Exception("qwe");
            }
        }

        public void AddPrivateQuestion(string LoginTo, string Question)
        {
            var userId = context.Users
                .Where(x => x.Login == LoginTo)
                .Select(x => x.UserId).FirstOrDefault();
            context.QuestionsAnswears
                .Add(new QandA
                {
                    UserId = userId,
                    Question = Question,
                    ToLogin = LoginTo,
                    FromLogin = "Anonymous"
                });

            context.SaveChanges();
        }

        public void AddPublicQuestion(string LoginTo, string LoginFrom, string Question)
        {
            var idqa = context.Users
                .Where(x => x.Login == LoginTo)
                .Select(x => x.UserId)
                .FirstOrDefault();
            var id = context.QuestionsAnswears
                .Where(x => x.UserId == idqa)
                .Count() + 1;

            var userId = context.Users
                .Where(x => x.Login == LoginTo)
                .Select(x => x.UserId).FirstOrDefault();
            context.QuestionsAnswears
                .Add(new QandA
                {
                    UserId = idqa,
                    Question = Question,
                    ToLogin = LoginTo,
                    FromLogin = LoginFrom
                });
            context.SaveChanges();
        }

        public void AddAnswear(int Id, string LoginTo, string Answear)
        {
            context.Users
                .Join(context.QuestionsAnswears, x => x.UserId, y => y.UserId, (x, y) => new { x, y })
                .Where(x => x.x.Login == LoginTo && x.y.IdQA == Id)
                .FirstOrDefault()
                .y.Answear = Answear;
            context.SaveChanges();
        }

        public void AddUserInfo(UserInfoViewModel userInfo, string picturePath)
        {
            var info = context.Users
                .Where(x => x.Login == userInfo.Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault();
            if (userInfo.FirstName != null)
                info.FirstName = userInfo.FirstName;
            if (userInfo.SecondName != null)
                info.SecondName = userInfo.SecondName;
            if (userInfo.BirthDate != null)
            {
                TimeSpan span = DateTime.Now - userInfo.BirthDate;
                info.Age = (byte)((new DateTime(1, 1, 1) + span).Year - 1);
            }
            info.PicturePath = picturePath;
            context.SaveChanges();
        }

        public void AddStatus(string login, string status)
        {
            context.Users
                .Where(x => x.Login == login)
                .Select(x => x.UserInfo)
                .FirstOrDefault()
                .Status = status;
            context.SaveChanges();
        }

        public bool Search(string login)
        {
            return context.Users.Any(x => x.Login == login);
        }

        public UserInfo GetUserInfo(string Login)
        {
            return context.Users
                .Where(x => x.Login == Login)
                .Select(x => x.UserInfo)
                .FirstOrDefault();
        }

        public List<User> GetAllUsers()
        {
            return context.Users.ToList();
        }

        public List<QuestionsModel> GetAllQuestions(string Login)
        {
            return context.Users
                    .Where(x => x.Login == Login)
                    .Join(context.QuestionsAnswears, x => x.UserId, y => y.UserId, (x, y) => y)
                    .OrderByDescending(x => x.IdQA)
                    .Select(x => new QuestionsModel
                    {
                        ImagePath =
                            context.Users.Where(y => y.Login == x.FromLogin)
                            .Select(z => z.UserInfo)
                            .FirstOrDefault()
                            .PicturePath,
                        Answear = x.Answear,
                        FromLogin = x.FromLogin,
                        IdQA = x.IdQA,
                        Question = x.Question,
                        ToLogin = x.ToLogin,
                        UserId = x.UserId
                    })
                    .ToList();
        }

        public List<QuestionsModel> GetAllQuestionsWithAnswears(string Login)
        {
            return context.Users
                    .Where(x => x.Login == Login)
                    .Join(context.QuestionsAnswears, x => x.UserId, y => y.UserId, (x, y) => y)
                    .Where(x => x.Answear != null)
                    .OrderByDescending(x => x.IdQA)
                    .Select(x => new QuestionsModel
                    {
                        ImagePath =
                            context.Users.Where(y => y.Login == x.FromLogin)
                            .Select(z => z.UserInfo)
                            .FirstOrDefault()
                            .PicturePath,
                        Answear = x.Answear,
                        FromLogin = x.FromLogin,
                        IdQA = x.IdQA,
                        Question = x.Question,
                        ToLogin = x.ToLogin,
                        UserId = x.UserId
                    })
                    .ToList();
        }

        public string GetJsonUsers(int id = 0, string written = "")
        {
            var res = GetUsersInJSON();
            if (id == 1)
                res = GetUsersInJSON(GetOrderedByAnswearsCount());
            if (id == 2)
                res = res.OrderBy(x => x.Login).ToList();
            if (id == 3)
                res = res.Where(x => x.Login.Contains(written)).Select(x => x).ToList();
            if (id == 4)
                res = GetUsersInJSON(context
                    .QuestionsAnswears
                    .Where(x => x.Question.Contains(written))
                    .Select(x => x.User)
                    .ToList());
            if (id == 5)
                res = GetUsersInJSON(context
                    .QuestionsAnswears
                    .Where(x => x.Answear.Contains(written))
                    .Select(x => x.User)
                    .ToList());
            return JsonConvert.SerializeObject(res, Formatting.Indented);

        }

        public List<User> GetOrderedByAnswearsCount()
        {
            return GetAllUsers()
                    .OrderByDescending(x => x.Questions.Select(y => y.Answear).Count())
                    .ThenBy(x => x.Login)
                    .ToList();
        }

        private List<UserModel> GetUsersInJSON(List<User> users = null)
        {
            List<User> user = GetAllUsers();
            if (users != null)
                user = users;
            return user.Select(x => new UserModel
            {
                Age = x.UserInfo.Age,
                FirstName = x.UserInfo.FirstName,
                Login = x.Login,
                PicturePath = x.UserInfo.PicturePath,
                SecondName = x.UserInfo.SecondName,
                Sex = x.UserInfo.Sex
            })
            .ToList();
        }
    }
}
