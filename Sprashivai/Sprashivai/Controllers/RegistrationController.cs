﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sprashivai.Data.Entities;
using Sprashivai.Data.Services;
using Sprashivai.Models.ViewModels;

namespace Sprashivai.Controllers
{
    [Route("[controller]/[action]")]
    public class RegistrationController : BaseController
    {
        public RegistrationController(DataBase dbOperations, FilesHandler filesHandler) : base(dbOperations, filesHandler)
        {
        }
        [Route("/registration")]
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [Route("/registration")]
        [HttpPost]
        public IActionResult Register(RegistrationViewModel registerModel)
        {
            if (ModelState.IsValid)
            {
                if (_dbOperations.CheckIfUserExists(registerModel.Login))
                {
                    ViewBag["exception"] = "Пользователь существует";
                    return View(registerModel);
                }
                _dbOperations.AddNewUser(registerModel);
                return RedirectToAction(nameof(RegisterAdditionalInfo), new { login = registerModel.Login });
            }
            return View(registerModel);
        }

        [Route("/registration/additional")]
        [HttpGet]
        public IActionResult RegisterAdditionalInfo(string login)
        {
            return View(new UserInfoViewModel { Login = login });
        }

        [Route("/registration/additional")]
        [HttpPost]
        public IActionResult RegisterAdditionalInfo(UserInfoViewModel userInfo)
        {
            if (ModelState.IsValid)
            {
                _dbOperations.AddUserInfo(userInfo);
                return RedirectToAction("Login", controllerName: "Login");
            }
            return View(userInfo);
        }
    }
}