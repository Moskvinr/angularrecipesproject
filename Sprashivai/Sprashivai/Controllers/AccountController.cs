﻿using Microsoft.AspNetCore.Mvc;
using Sprashivai.Data.Services;
using Sprashivai.Models;
using Sprashivai.Models.ViewModels;
using System.Collections.Generic;

namespace Sprashivai.Controllers
{
    public class AccountController : BaseController
    {
        public AccountController(DataBase dbOperations, FilesHandler filesHandler) : base(dbOperations, filesHandler)
        {
        }

        public IActionResult Index(string login)
        {
            var thisUser = HttpContext.Request.Cookies["login"];
            if (_dbOperations.CheckIfUserExists(login))
            {
                var accountModel = new AccountPageViewModel
                {
                    Login = thisUser,
                    Status = _dbOperations.GetStatus(login),
                    ImagePath = _dbOperations.GetImagePath(login),
                    UserInfo = _dbOperations.GetUserInfo(login),
                };
                List<QuestionsModel> questions = new List<QuestionsModel>();
                if (thisUser == login)
                {
                    questions = _dbOperations.GetAllQuestions(login);
                    accountModel.Questions = questions;
                    accountModel.Count = questions.Count;
                    return View("UserPage", accountModel);
                }
                questions = _dbOperations.GetAllQuestionsWithAnswears(login);
                accountModel.Questions = questions;
                accountModel.Count = questions.Count;
                return View("GuestPage", accountModel);
            }
            return RedirectToAction(nameof(Index), new { login = thisUser });
        }

        [HttpGet]
        public IActionResult Settings()
        {
            var userInfo = _dbOperations
                .GetUserInfo(HttpContext.Request.Cookies["login"]);
            if (userInfo == null)
            {
                return RedirectToAction("Login", "Login");
            }
            return View(userInfo);
        }

        [HttpPost]
        public IActionResult Settings(UserInfoViewModel userInfo)
        {
            if (ModelState.IsValid)
            {
                _dbOperations.AddUserInfo(userInfo, _filesHandler.CreateFile(userInfo.PicturePath));
                RedirectToAction(nameof(Index));
            }
            return View(userInfo);
        }

        [HttpPost]
        public IActionResult AskUser(AskModel askModel)
        {
            if (ModelState.IsValid)
            {
                if (askModel.IsAnonymous)
                    _dbOperations.AddPrivateQuestion(askModel.LoginTo, askModel.Question);
                else
                    _dbOperations.AddPublicQuestion(askModel.LoginTo, HttpContext.Request.Cookies["login"], askModel.Question);
            }
            return RedirectToAction(nameof(Index), new { login = askModel.LoginTo });
        }

        [HttpPost]
        public IActionResult ChangeStatus(string status)
        {
            var userLogin = HttpContext.Request.Cookies["login"];
            _dbOperations.AddStatus(status, userLogin);
            return RedirectToAction(nameof(Index), new { login = userLogin });
        }

        [HttpPost]
        public IActionResult AddAnswear(int questionId, string answear)
        {
            var login = HttpContext.Request.Cookies["login"];
            _dbOperations.AddAnswear(questionId, login , answear);
            return RedirectToAction(nameof(Index), new { login });
        }

        [HttpPost]
        public IActionResult Logout()
        {
            HttpContext.Response.Cookies.Delete("login");
            return RedirectToAction("Login", "Login");
        }
    }
}