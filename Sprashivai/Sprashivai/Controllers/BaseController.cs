﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sprashivai.Data.Services;

namespace Sprashivai.Controllers
{
    public class BaseController : Controller
    {
        protected DataBase _dbOperations;
        protected FilesHandler _filesHandler;

        public BaseController(DataBase dbOperations, FilesHandler filesHandler)
        {
            _dbOperations = dbOperations;
            _filesHandler = filesHandler;
        }
    }
}