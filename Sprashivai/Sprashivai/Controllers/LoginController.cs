﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sprashivai.Data.Services;
using Sprashivai.Models.ViewModels;

namespace Sprashivai.Controllers
{
    [Route("[controller]/")]
    public class LoginController : BaseController
    {
        public LoginController(DataBase dbOperations, FilesHandler filesHandler) : base(dbOperations, filesHandler)
        {
        }

        [HttpGet]
        public IActionResult Login()
        {
            var userLogin = Request.Cookies["login"];
            if (userLogin != null &&_dbOperations.CheckIfUserExists(userLogin))
            {
                return RedirectToAction("Index", "Account", new { login = userLogin });
            }
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {
                if (_dbOperations.CheckPass(loginModel.Login, loginModel.Password))
                {
                    HttpContext.Response.Cookies.Append("login", loginModel.Login);
                    return RedirectToAction("Index", "Account", new { login = loginModel.Login });
                }
            }
            return View(loginModel);
        }
    }
}