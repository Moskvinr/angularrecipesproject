import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login-service/login.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private auth: LoginService) { }

  isLoggedOn(): boolean {
    return this.auth.loggedOn;
  }

  logOut() {
    this.auth.clear();
  }

  ngOnInit() {
  }

}
