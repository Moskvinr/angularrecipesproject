import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from '../register/register.component';
import { FormsModule } from '@angular/forms';

const registerRoutes: Routes = [
  {
    path: '',
    component: RegisterComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(registerRoutes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
