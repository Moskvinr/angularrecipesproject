import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { LoginService } from '../../login/login-service/login.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public userModel: User = new User(null, null);

  constructor(private authService: LoginService, private router: Router) { }

  register(form: NgForm) {
      this.authService.addNewUser(this.userModel);
      this.router.navigateByUrl('/signin');
  }

  ngOnInit() {
  }

}
