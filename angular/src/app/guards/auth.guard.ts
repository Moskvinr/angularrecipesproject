import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from '../login/login-service/login.service';

@Injectable()
export class AuthGuard {

    constructor(private router: Router, private auth: LoginService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if (this.auth.loggedOn) {
            return true;
        }

        this.router.navigateByUrl('/signin');
        return false;
    }
}
