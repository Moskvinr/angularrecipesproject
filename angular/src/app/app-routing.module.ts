import { RouterModule, PreloadingStrategy, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { SelectivePreloadingStrategy } from './extensions/preloading-strategy';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login/login.component';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'recipes', loadChildren: './recipes/recipe/recipe.module#RecipeModule', canActivate: [AuthGuard] },
  { path: 'signin', loadChildren: './login/login-module/login-module.module#LoginModuleModule' },
  {
    path: 'shopping-list',
    loadChildren: './shoppinglist/shopping-list-module/shoppinglist.module#ShoppinglistModule', canActivate: [AuthGuard]
  },
  { path: 'signup', loadChildren: './register/register.module#RegisterModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false,
        preloadingStrategy: SelectivePreloadingStrategy,
      }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
    SelectivePreloadingStrategy, AuthGuard
  ]
})
export class AppRoutingModule { }
