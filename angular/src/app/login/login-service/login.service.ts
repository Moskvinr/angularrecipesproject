import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Login } from '../login';
import { User } from '../../models/user';
import { map } from 'rxjs/operators';

const USERS: User[] = [
  { id: 1, username: 'first', password: '123' },
  { id: 2, username: 'second', password: '123' },
];

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  auth_token: string;

  constructor() { }

  login(login: Login): Observable<boolean> {
    if (USERS.some(user => user.username.toLowerCase().trim() === login.username.toLowerCase().trim()
      && user.password === login.password)) {
        this.auth_token = 'token' + Math.random() * 10;
        return of(true);
    }
    return of(false);
  }

  addNewUser(user: User) {
    user.id = User.nextId++;
    USERS.push(user);
  }

  get loggedOn(): boolean {
    return this.auth_token != null;
  }

  clear() {
    this.auth_token = null;
  }

}
