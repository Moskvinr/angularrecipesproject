import { Component, OnInit } from '@angular/core';
import { Login } from '../login';
import { LoginService } from '../login-service/login.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public loginModel: Login = new Login();
  public errorMessage: string;

  constructor(private authService: LoginService, private router: Router) { }

  authenticate(form: NgForm) {

    if (form.valid) {
      this.authService.login(this.loginModel)
        .subscribe(resp => {
          if (resp) {
            this.router.navigateByUrl('');
          }
          this.errorMessage = 'bad login or password';
        });
    } else {
      this.errorMessage = 'smthng went wrong(';
    }
  }

}
