import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpModule } from '@angular/http';
import { ShoppingListService } from './shoppinglist/shopping-service/shopping-list.service';
import { DialogService } from './extensions/dialog.service';
import { LoginService } from './login/login-service/login.service';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent
  ],
  imports: [
    NgbModule,
    AppRoutingModule,
    HttpModule,
    BrowserModule
  ],
  providers: [ShoppingListService, DialogService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
