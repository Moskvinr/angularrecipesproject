import { Injectable } from '@angular/core';
import { Ingredients } from '../../models/ingredients';
import { BehaviorSubject, Observable } from 'rxjs';

const INGREDIENTS: Ingredients[] = [
  new Ingredients('firstIngred', 5),
  new Ingredients('secondIngred', 10)
];

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  private ingredientsList: BehaviorSubject<Ingredients[]> = new BehaviorSubject<Ingredients[]>(INGREDIENTS);

  constructor() { }

  private checkForSame(ingredient: Ingredients): boolean {
    return INGREDIENTS.some(x => x.name.toLowerCase() === ingredient.name.toLowerCase());
  }

  private getIndexOfSame(ingredient: Ingredients): number {
    return INGREDIENTS.indexOf(INGREDIENTS.find(x => x.name.toLowerCase() === ingredient.name.toLowerCase()));
  }

  deleteIngredientFromShopping(ingredient: Ingredients) {
    if (this.checkForSame(ingredient)) {
      let indexOfSame = this.getIndexOfSame(ingredient);
      INGREDIENTS[indexOfSame].amount -= ingredient.amount;
      if (INGREDIENTS[indexOfSame].amount < 1) {
        let ingrForDelete = INGREDIENTS[indexOfSame];
        INGREDIENTS.splice(INGREDIENTS.indexOf(ingrForDelete), 1);
      }
    }
  }

  getIngredientsForShopping(): Observable<Ingredients[]> {
    return this.ingredientsList;
  }

  addIngredientToShopping(ingredient: Ingredients) {
    ingredient.name = ingredient.name.trim().toLowerCase();
    if (this.checkForSame(ingredient)) {
      INGREDIENTS[this.getIndexOfSame(ingredient)].amount += ingredient.amount;
    } else {
      INGREDIENTS.push(new Ingredients(ingredient.name, ingredient.amount));
    }
  }

}
