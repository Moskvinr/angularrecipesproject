import { Component, OnInit } from '@angular/core';
import { Ingredients } from '../../models/ingredients';
import { Observable, of } from 'rxjs';
import { ShoppingListService } from '../shopping-service/shopping-list.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  ingredients: Observable<Ingredients[]>;

  constructor(private shoppingService: ShoppingListService) { }

  add(form: NgForm) {
    this.shoppingService.addIngredientToShopping(this.getIngredient(form));
    this.clear(form);
  }

  private getIngredient(form: NgForm): Ingredients {
    return new Ingredients(form.value.name, <number>form.value.amount);
  }

  delete(form: NgForm) {
    this.shoppingService.deleteIngredientFromShopping(this.getIngredient(form));
    this.clear(form);
  }

  checkIfAnyIngred(form: NgForm): boolean {
    let flag: boolean;
    this.ingredients
      .subscribe({
        next(ingredient) {
          flag = ingredient.some(y => y.name === form.value.name);
        }
      });
    return flag;
  }

  fillFields(form: NgForm, ingredient: Ingredients) {
    form.controls['name'].setValue(ingredient.name);
    form.controls['amount'].setValue(ingredient.amount);
  }

  clear(form: NgForm) {
    form.resetForm();
  }

  ngOnInit() {
    this.ingredients = this.shoppingService.getIngredientsForShopping();
  }

}
