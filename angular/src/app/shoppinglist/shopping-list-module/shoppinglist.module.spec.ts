import { ShoppinglistModule } from './shoppinglist.module';

describe('ShoppinglistModule', () => {
  let shoppinglistModule: ShoppinglistModule;

  beforeEach(() => {
    shoppinglistModule = new ShoppinglistModule();
  });

  it('should create an instance', () => {
    expect(shoppinglistModule).toBeTruthy();
  });
});
