import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppinglistRoutingModule } from '../shoppinglist-routing/shoppinglist-routing.module';
import { ShoppingListComponent } from '../shopping-list/shopping-list.component';
import { ShoppingListService } from '../shopping-service/shopping-list.service';
import { NgForm, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ShoppinglistRoutingModule,
    FormsModule
  ],
  declarations: [
    ShoppingListComponent
  ],
  providers: []
})
export class ShoppinglistModule { }
