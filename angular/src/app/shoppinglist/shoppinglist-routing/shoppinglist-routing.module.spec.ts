import { ShoppinglistRoutingModule } from './shoppinglist-routing.module';

describe('ShoppinglistRoutingModule', () => {
  let shoppinglistRoutingModule: ShoppinglistRoutingModule;

  beforeEach(() => {
    shoppinglistRoutingModule = new ShoppinglistRoutingModule();
  });

  it('should create an instance', () => {
    expect(shoppinglistRoutingModule).toBeTruthy();
  });
});
