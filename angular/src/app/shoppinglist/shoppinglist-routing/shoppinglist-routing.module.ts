import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingListComponent } from '../shopping-list/shopping-list.component';

const shoppingListRouting: Routes = [
  {
    path: '', component: ShoppingListComponent, pathMatch: 'full'
  }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(shoppingListRouting)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class ShoppinglistRoutingModule { }
