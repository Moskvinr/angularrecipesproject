import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipeComponent } from './recipe.component';
import { RecipeListComponent } from '../recipe-list/recipe-list.component';
import { RecipeDetailsComponent } from '../recipe-details/recipe-details.component';
import { RecipeService } from '../services/recipe.service';
import { RecipeRoutingModule } from '../recipe-routing/recipe-routing.module';
import { NewRecipeComponent } from '../new-recipe/new-recipe.component';
import { EditRecipeComponent } from '../edit-recipe/edit-recipe.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RecipeRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    RecipeComponent,
    RecipeListComponent,
    RecipeDetailsComponent,
    NewRecipeComponent,
    EditRecipeComponent
  ],
  providers: [RecipeService]
})
export class RecipeModule { }
