import { Ingredients } from '../../models/ingredients';

export class Recipe {
    // tslint:disable-next-line:max-line-length
    constructor(public id: number, public name: string, public description: string, public image: string, public ingredients: Ingredients[]) { }
  }
