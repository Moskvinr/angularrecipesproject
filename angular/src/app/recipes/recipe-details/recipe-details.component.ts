import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../services/recipe.service';
import { Recipe } from '../recipe/recipe';
import { Ingredients } from '../../models/ingredients';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {

  recipe: Recipe;

  constructor(
    private route: ActivatedRoute,
    private rS: RecipeService,
    private router: Router
  ) { }

  delete(id: number) {
    this.rS.deleteRecipe(id);
    this.router.navigate(['/recipes']);
  }

  addToShoppingList(ingredients: Ingredients[]) {
    this.rS.addToShoppingList(ingredients);
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: {recipe: Recipe}) => {
        this.recipe = data.recipe;
      });
  }

}
