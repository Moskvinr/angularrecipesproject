import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { RecipeService } from '../services/recipe.service';
import { FormGroup, FormBuilder, FormArray, NgForm } from '@angular/forms';
import { Recipe } from '../recipe/recipe';
import { Ingredients } from '../../models/ingredients';
import { Observable } from 'rxjs';
import { DialogService } from '../../extensions/dialog.service';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit {


  name: string;
  description: string;
  imageUrl: string;

  recipeForm: FormGroup;
  updatableRecipe: Recipe;
  constructor(
    private route: ActivatedRoute,
    private rS: RecipeService,
    private router: Router,
    private fb: FormBuilder,
    private dialog: DialogService) { }

  ngOnInit() {
    this.route.data
      .subscribe((data: { recipe: Recipe }) => {
        this.updatableRecipe = data.recipe;
      });
    this.imageUrl = this.updatableRecipe.image;
    this.name = this.updatableRecipe.name;
    this.description = this.updatableRecipe.description;

    this.fillForm();
  }

  save() {
    const ingredientsValues: Ingredients[] = this.recipeForm.get('ingredients').value;
    this.updatableRecipe.ingredients.length = 0;
    this.updatableRecipe.ingredients = ingredientsValues;
    this.rS.updateRecipe(this.updatableRecipe);
    this.navigateBack(this.updatableRecipe.id);
  }

  navigateBack(id: number) {
    this.router.navigateByUrl('recipes/' + id);
  }

  cancel() {
    this.updatableRecipe.name = this.name;
    this.updatableRecipe.description = this.description;
    this.updatableRecipe.image = this.imageUrl;
    this.navigateBack(this.updatableRecipe.id);
  }

  fillForm() {
    this.recipeForm = this.fb.group({
      name: [this.updatableRecipe.name],
      description: [this.updatableRecipe.description],
      image: [this.updatableRecipe.image],
      ingredients: this.fb.array([])
    });
    for (let ingredient of this.updatableRecipe.ingredients) {
      this.fillIngredient(ingredient.name, ingredient.amount);
    }
  }

  get ingredients() {
    return this.recipeForm.get('ingredients') as FormArray;
  }

  addIngredient() {
    this.ingredients.push(this.fb.group({
      name: [''],
      amount: []
    }));
  }

  deleteIngredient() {
    if (this.ingredients.length > 1) {
      this.ingredients.removeAt(this.ingredients.length - 1);
    }
  }

  fillIngredient(name: string, amount: number) {
    this.ingredients.push(this.fb.group({
      name: [name],
      amount: [amount]
    }));
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.updatableRecipe.name === this.name &&
      this.updatableRecipe.description === this.description &&
      this.updatableRecipe.image === this.imageUrl) {
      return true;
    }

    return this.dialog.confirm('go back without savechanges?');
  }

}
