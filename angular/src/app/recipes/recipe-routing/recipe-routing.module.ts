import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipeDetailsComponent } from '../recipe-details/recipe-details.component';
import { RecipeListComponent } from '../recipe-list/recipe-list.component';
import { RecipeComponent } from '../recipe/recipe.component';
import { RouterModule, Routes } from '@angular/router';
import { RecipeDetailResolverService } from '../services/recipe-detail-resolver.service';
import { NewRecipeComponent } from '../new-recipe/new-recipe.component';
import { EditRecipeComponent } from '../edit-recipe/edit-recipe.component';

const recipeRouting: Routes = [
  {
    path: '', component: RecipeComponent, children: [
      {
        path: '',
        component: RecipeListComponent,
        children: [
          {
            path: 'new',
            component: NewRecipeComponent
          },
          {
            path: ':id',
            component: RecipeDetailsComponent,
            resolve: {
              recipe: RecipeDetailResolverService
            }
          },
          {
            path: ':id/edit',
            component: EditRecipeComponent,
            resolve: {
              recipe: RecipeDetailResolverService
            }
          }]
      },

    ]
  }
];

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(recipeRouting)
  ],
  declarations: [],
  exports: [RouterModule],
  providers: [RecipeDetailResolverService]
})
export class RecipeRoutingModule { }
