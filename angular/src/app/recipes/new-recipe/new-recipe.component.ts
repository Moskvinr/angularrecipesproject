import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecipeService } from '../services/recipe.service';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Recipe } from '../recipe/recipe';
import { Ingredients } from '../../models/ingredients';

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})
export class NewRecipeComponent implements OnInit {


  recipeForm: FormGroup;
  newRecipe: Recipe;

  constructor(
    private route: ActivatedRoute,
    private rS: RecipeService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.recipeForm = this.fb.group({
      name: [''],
      description: [''],
      image: [''],
      ingredients: this.fb.array([])
    });
    this.addIngredient();
  }

  save() {
    // tslint:disable-next-line:max-line-length
    let recipe = new Recipe(0, this.recipeForm.get('name').value, this.recipeForm.get('description').value,
      this.recipeForm.get('image').value,
      <Ingredients[]>this.recipeForm.get('ingredients').value);
    this.rS.addRecipe(recipe);
    this.navigateBack();
  }

  cancel() {
    this.navigateBack();
  }
  navigateBack() {
    this.router.navigateByUrl('/recipes');
  }

  get ingredients() {
    return this.recipeForm.get('ingredients') as FormArray;
  }

  addIngredient() {
    this.ingredients.push(this.fb.group({
      name: [''],
      amount: []
    }));
  }

  deleteIngredient() {
    if (this.ingredients.length > 1) {
      this.ingredients.removeAt(this.ingredients.length - 1);
    }
  }

}

