import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { RecipeService } from '../services/recipe.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Recipe } from '../recipe/recipe';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Observable<Recipe[]>;
  selectedId: number;

  constructor(private rS: RecipeService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.recipes = this.route.paramMap.pipe(
      switchMap((param: ParamMap) => {
        this.selectedId = +param.get('id');
        return this.rS.getRecipes();
      })
    );
  }

}
