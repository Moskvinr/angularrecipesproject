import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RecipeService } from './recipe.service';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Recipe } from '../recipe/recipe';

@Injectable(
)
export class RecipeDetailResolverService implements Resolve<Recipe> {

  constructor(private rS: RecipeService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> {
    let id = route.paramMap.get('id');

    return this.rS.getRecipe(id).pipe(
      take(1),
      map(recipe => {
        if (recipe) {
          return recipe;
        } else {
          this.router.navigate(['/recipes']);
          return null;
        }
      })
    );

  }

}
