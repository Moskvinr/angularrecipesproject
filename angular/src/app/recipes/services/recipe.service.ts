import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Recipe } from '../recipe/recipe';
import { Ingredients } from '../../models/ingredients';
import { ShoppingListService } from '../../shoppinglist/shopping-service/shopping-list.service';

const RECIPES: Recipe[] = [
  // tslint:disable-next-line:max-line-length
  { id: 1, name: 'test', description: 'test', image: 'https://images.unsplash.com/photo-1531750985903-9018577a2472?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6cbc5e7fab32db22814e5e99953534be&auto=format&fit=crop&w=634&q=80', ingredients: [{ name: 'first', amount: 5 }] },
  // tslint:disable-next-line:max-line-length
  { id: 2, name: 'secTest', description: 'secTest', image: 'https://images.unsplash.com/photo-1531750985903-9018577a2472?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6cbc5e7fab32db22814e5e99953534be&auto=format&fit=crop&w=634&q=80', ingredients: [{ name: 'second', amount: 10 }] }
];


@Injectable()
export class RecipeService {
  static nextRecipeId = 5;
  private recipes: BehaviorSubject<Recipe[]> = new BehaviorSubject<Recipe[]>(RECIPES);
  private recipe: Recipe;

  getRecipes() {
    return this.recipes;
  }

  getRecipe(id: number | string) {
    return this.getRecipes().pipe(
      map(recipes => recipes.find(recipe => recipe.id === +id))
    );
  }

  deleteRecipe(id: number | string) {
    this.getRecipe(id).subscribe(recipe => this.recipe = recipe);
    if (this.recipe != null) {
      RECIPES.splice(RECIPES.indexOf(this.recipe), 1);
    }
  }

  addToShoppingList(ingredient: Ingredients[]) {
    ingredient.forEach(x => this.shoppingListService.addIngredientToShopping(x));
  }

  updateRecipe(recipe: Recipe) {
    this.getRecipe(recipe.id).subscribe(item => this.recipe = item);
    RECIPES.splice(RECIPES.indexOf(this.recipe), 1, recipe);
  }

  addRecipe(recipe: Recipe) {
    recipe.id = RecipeService.nextRecipeId++;
    RECIPES.push(recipe);
  }

  constructor(private shoppingListService: ShoppingListService) { }
}
